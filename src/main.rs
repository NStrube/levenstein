use std::path::PathBuf;
use std::process::ExitCode;
use std::io::{self, BufRead, BufReader};
use std::fs::File;

fn main() -> io::Result<ExitCode> {
    let mut args = std::env::args();
    let pname = args.next().unwrap();

    let (p1, p2) = match (args.next(), args.next()) {
	(Some(s1), Some(s2)) => (PathBuf::from(s1), PathBuf::from(s2)),
	_ => {
	    eprintln!("Usage: {} <file1> <file2>", pname);
	    return Ok(ExitCode::FAILURE);
	}
    };

    let (f1, f2) = (File::open(&p1)?, File::open(&p2)?);
    let (f1, f2) = (BufReader::new(f1), BufReader::new(f2));

    for (i, (l1, l2)) in f1.lines().zip(f2.lines()).enumerate() {
	let (l1, l2) = (l1?, l2?);

	if two_row(l1.as_bytes(), l2.as_bytes()) != 0 {
	    println!("'{}' - '{}':{}:", p1.display(), p2.display(), i+1);
	    println!("-{}", l1);
	    println!("+{}", l2);
	}
    }

    Ok(ExitCode::SUCCESS)
}

fn naive<T: PartialEq>(a: &[T], b: &[T]) -> usize {
    if a.len() == 0 {
        return b.len();
    }

    if b.len() == 0 {
        return a.len();
    }

    if a[0] == b[0] {
        return naive(&a[1..], &b[1..]);
    }

    1 + [
        naive(&a[1..], b),
        naive(a, &b[1..]),
        naive(&a[1..], &b[1..]),
    ]
    .iter()
    .min()
    .unwrap()
}

fn matrix<T: PartialEq>(a: &[T], b: &[T]) -> usize {
    let m = a.len();
    let n = b.len();

    let mut matrix = vec![vec![0; n + 1]; m + 1];
    for i in 1..=m {
        matrix[i][0] = i;
    }
    for i in 1..=n {
        matrix[0][i] = i;
    }

    for j in 1..=n {
        for i in 1..=m {
            let cost = if a[i-1] == b[j-1] { 0 } else { 1 };
            matrix[i][j] = [
                matrix[i - 1][j] + 1,
                matrix[i][j - 1] + 1,
                matrix[i - 1][j - 1] + cost,
            ]
            .iter()
            .copied()
            .min()
            .unwrap();
        }
    }

    matrix[m][n]
}

fn two_row<T: PartialEq>(a: &[T], b: &[T]) -> usize {
    let m = a.len();
    let n = b.len();

    let mut v0 = vec![0; n + 1];
    let mut v1 = vec![0; n + 1];

    for i in 0..=n {
        v0[i] = i;
    }

    for i in 0..m {
        v1[0] = i + 1;

        for j in 0..n {
            let deletion_cost = v0[j + 1] + 1;
            let insertion_cost = v1[j] + 1;
            let substition_cost = if a[i] == b[j] { v0[j] } else { v0[j] + 1 };

            v1[j + 1] = [deletion_cost, insertion_cost, substition_cost]
                .iter()
                .copied()
                .min()
                .unwrap();
        }

        std::mem::swap(&mut v0, &mut v1);
    }

    return v0[n];
}

#[cfg(test)]
mod test {
    const TESTS: &[(&str, &str, usize)] = &[
	("abc", "abc", 0),
	("abc", "abd", 1),
	("acc", "abc", 1),
	("", "abc", 3),
	("hello", "there", 4),
	("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non justo orci. Pellentesque egestas nulla id sapien luctus dictum. Nullam mattis augue eget euismod ultricies. Nunc vel ante laoreet, volutpat nisi a, consequat risus. Vivamus nec augue vitae magna aliquet luctus. Vivamus consectetur, lorem convallis lacinia rhoncus, odio nulla lobortis arcu, sit amet laoreet risus diam quis turpis. Mauris id neque quis arcu condimentum molestie. In hac habitasse platea dictumst. Curabitur sed purus et urna ornare rhoncus sit amet sed mauris. Phasellus sollicitudin ligula velit, at pulvinar ex luctus aliquet. Curabitur porttitor, nulla eu rutrum luctus, erat sem eleifend felis, in ultrices ante justo eu massa. Aliquam erat volutpat. Duis nec nunc laoreet, tristique est ac, pharetra justo. Nullam ut eleifend metus, at convallis lectus. Phasellus porta fermentum ante ut lobortis. Proin lacinia porttitor nibh in iaculis.",
	 "Donec vehicula mi in sapien convallis eleifend. Suspendisse a massa elit. Fusce a odio vel est iaculis posuere et in tortor. Pellentesque consequat erat leo, a commodo nisl aliquet non. Vivamus ultrices ipsum at sem tempor, id hendrerit tellus aliquam. Integer egestas nulla et leo faucibus consequat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque ullamcorper non nulla in cursus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec quis nisi nec lacus ultrices sodales facilisis a urna. Praesent vitae lacus facilisis, aliquam lectus quis, scelerisque arcu.", 656),
    ];

    #[test]
    fn naive() {
	for &(s1, s2, n) in TESTS {
	    assert_eq!(super::naive(s1.as_bytes(), s2.as_bytes()), n);
	}
    }

    #[test]
    fn matrix() {
	for &(s1, s2, n) in TESTS {
	    assert_eq!(super::matrix(s1.as_bytes(), s2.as_bytes()), n);
	}
    }

    #[test]
    fn two_row() {
	for &(s1, s2, n) in TESTS {
	    assert_eq!(super::two_row(s1.as_bytes(), s2.as_bytes()), n);
	}
    }
}
